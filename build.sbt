name := "CS441HW1"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % "1.3.2"
)
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.7"

libraryDependencies += "com.novocode" % "junit-interface" % "0.8" % "test->default"
testOptions += Tests.Argument(TestFrameworks.JUnit, "-v")