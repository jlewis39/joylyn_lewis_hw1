### CS 441 HW1 - Create cloud simulators for evaluating executions of applications in cloud datacenters with different characteristics and deployment models
### Submitted By - Joylyn Lewis

### Details of important files included in the directory structure of the forked private repository:
- /lib folder contains the CloudSim relevant packages **cloudsim-3.0.3.jar** and **commons-math3-3.6.1.jar**
- /src/main/resources folder contains the files **CloudProvider1.conf, CloudProvider2.conf, CloudProvider3.conf** and **logback.xml**
    - Each of the *.conf files includes values of different characteristics of various cloud resources and cloudlets that can be assumed for each of the three cloud providers Google, Amazon and Microsoft respectively.
    - The logback.xml file sets up the ConsoleAppender and FileAppender details which enable logging statements onto console and file. A file named 'LogFile.log' gets generated with the logging statements.
- /src/main/scala/com/cs441/hw1 folder contains the files **Simulator1.scala, Simulator2.scala** and **Simulator3.scala**. Each of these files represents a simulator program that models a cloud environment and considers 
    inputs from the configuration files CloudProvider1.conf, CloudProvider2.conf, CloudProvider3.conf respectively.
- /src/test/scala/com/cs441/hw1 folder contains the files **Simulator1Test.scala, Simulator2Test.scala** and **Simulator3Test.scala**. Each of these files contain five Junit Test cases to test the three simulator 
    programs Simulator1.scala, Simulator2.scala and Simulator3 respectively.
- /**build.sbt** provides dependencies on libraries required for enabling logging statements and writing Junit test cases  

**NOTE:** In additon to the above files related to the code, files **'Simulator 1 Results and Explanation'.docx, 'Simulator 2 Results and Explanation'.docx, 'Simulator 3 Results and Explanation'.docx** provide details of 
the results and evaluations of the three simulators

### How to run the code
- Import the project structure from the forked repository **https://bitbucket.org/jlewis39/joylyn_lewis_hw1/src/master/** into IntelliJ
- If importing from IntelliJ -> you can clone it via VCS > Checkout from Version Control > Git. Use the URL highlighted above for the repository on bitbucket
- To run the programs in IntelliJ
    - Locate the file Simulator1.scala in the folder \src\main\scala\com\cs441\hw1\Simulator1.scala. Run the program. The output results will appear on the console and 
        file 'LogFile.log' with the logging statements will be generated
    - Similalary locate files Simulator2.scala and Simulator3.scala in the same directory and run the files one after the other to see the outputs on console and in the 'LogFile.log' that is generated
    - To test the three simulators, locate the file Simulator1Test.scala in the folder \src\test\scala\com\cs441\hw1\Simulator1Test.scala. Run the test program. The tests passed will appear on console
    - Similarly locate files Simulator2Test.scala and Simulator3Test.scala in the same directory and run the files one after the other to see the tests passed on the console.
- To run the program using SBT
    - In command line, go to the project directory
    - Enter command 'sbt run'. As there are three simulator scala objects and each object has a main method, the below message will appear:
        Multiple main classes detected, select one to run:   
        [1] com.cs441.hw1.Simulator1  
        [2] com.cs441.hw1.Simulator2  
        [3] com.cs441.hw1.Simulator3  
    - Please enter number '1' at command line to run Simulator1.scala program. The output will appear on the console. 'LogFile.log' with the logging details will be generated.
    - Similarly, enter command 'sbt run' and select number 2 to execute program Simulator2.scala. The output will appear on the console. 'LogFile.log' with the logging details will be generated.
    - Similarly, enter command 'sbt run' and select number 3 to execute program Simulator3.scala. The output will appear on the console. 'LogFile.log' with the logging details will be generated.
    - To execute the JUnit Test cases, enter the command 'sbt test' on the command line. The number of tests run and passed will appear on the console.
    
###Approach towards designing the three simulators

####Assume that a cloud customer has two requirements:
- Execute a set of 15 applications that can be categorized into three types - first 5 represent small scale applications, next 5 represent medium scale applications and the last 5 represent large scale applications
- Execute a set of 4 applications that can be categorized into two types - first 2 represent small scale applications and next 2 represent large scale applications

Now, we have 3 cloud providers namely Google, Amazon and Microsoft each of which have characterisitcs of their resources defined in the 3 config files CloudProvider1.conf, CloudProvider2.conf and CloudProvider3.conf respectively.
Although all configuration variables can be provided in a single configuration file, the approach implemented here is to have 3 configuration files, one for each cloud provider so that the values can be easily tracked and maintained.

In addition to the functionalities provided by CloudSim, below are enhancements done to the three Simulators:  

- **Simulator 1**:
    - A custom VM Allocation poliy is designed which chooses the host with minimum average CPU Utilization, as the host to be allocated for a VM. 
    - Total processing cost of cloudlet is determined by enhancing the existing funtion which sums up only the input and output transfer cost to also include the cost of executing the cloudlet

- **Simulator 2**:
    - A load balancing concept is attempted by creating a custom datacenter broker that allocates higher length cloudlets to higher capacity VMS and lower length cloudlets to lower capacity VMS
    - Total processing cost of cloudlet is determined by enhancing the existing funtion which sums up only the input and output transfer cost to also include the cost of executing the cloudlet

- **Simulator 3**:
    - Total processing cost of cloudlet is determined by enhancing the existing funtion which sums up only the input and output transfer cost to also include the cost of executing the cloudlet

Addiitonal details of each of the simulators including their results and evaluation is provided in the individual files 'Simulator 1 Results and Explanation'.docx, 'Simulator 2 Results and Explanation'.docx, 'Simulator 3 Results and Explanation'.docx 
respectively that are uploaded in this repo.

    
    