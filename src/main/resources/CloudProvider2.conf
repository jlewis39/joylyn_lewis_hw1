//Characteristics for Amazon Cloud Provider used in Simulator 2

//Provides general parameters alongwith output patterns

CloudSim{
  noOfCloudUsers = 1                                 //number of cloud users
  traceFlag = false                                  //flag for trace events
  logger = 'Simulator2'                              //name of logger instance
  brokerName = "Broker"                              //name of broker object
  cloudletMin = 1                                    //minimum number of required cloudlets
  cloudletMax = 4                                    //maximum number of required cloudlets
  decimalRoundOff = 2                                //number of decimal places required in calculations of type Double
  success = "SUCCESS"                                //indicate success status of cloudlet
  fail = "FAIL"                                      //indicate fail status of cloudlet
  //Patterns while logging output results of cloudlets
  indent = "    "
  output = "========== OUTPUT =========="
  pattern = "###.##"
}

//Provides general datacenter parameters
DatacenterParameters{
  peMin = 1                                         //minimum number of required PEs
  peMax = 4                                         //maximum number of required PEs
  mips = 5000                                       //mips value
  hostMin = 1                                       //minimum number of required hosts
  hostMax = 2                                       //maximum number of required hosts
  vmMin = 1                                         //minimum number of required VMs
  vmMax = 2                                         //maximum number of required VMs
  schedulingInterval = 0                            //scheduling interval
  Name1 = "Datacenter_1"                            //Name of first datacenter
  Name2 = "Datacenter_2"                            //Name of second datacenter
}

//Provides characteristics of datacenter #1
Datacenter_1{
  ram = 512                                         //host memory (MB)
  storage = 1050000                                 //host storage
  bw = 10500                                        //bandwidth
  arch = "x86"                                      //system architecture
  os = "Linux"                                      //operating system
  vmm = "Xen"                                       //VMM name
  timeZone = 10.0                                   //time zone this resource is located
  cost = 1.0                                        //the cost of using processing in this resource
  costPerMem = 0.004	                              //the cost of using memory in this resource
  costPerStorage = 0.0002	                          //the cost of using storage in this resource
  costPerBw = 0.003                                 //the cost of using bw in this resource
}

//Provides characteristics of datacenter #2
Datacenter_2{
  ram = 10240                                       //host memory (MB)
  storage = 1000000                                 //host storage
  bw = 10000                                        //bandwidth
  arch = "x86"                                      //system architecture
  os = "Linux"                                      //operating system
  vmm = "Xen"                                       //VMM name
  timeZone = 1.0                                    //time zone this resource located
  cost = 2.0                                        //the cost of using processing in this resource
  costPerMem = 0.003                                //the cost of using memory in this resource
  costPerStorage = 0.0001                           //the cost of using storage in this resource
  costPerBw = 0.004                                 //the cost of using bw in this resource
}

//Provides characteristics of virtual machine #1
Vm1{
  mips = 400                                        //mips value
  pesNumber = 2                                     //number of cpus
  ram = 512                                         //vm memory (MB)
  bw = 1000                                         //bandwidth
  size = 10000                                      //image size (MB)
  vmm = "Xen"                                       //VMM name
}

//Provides characteristics of virtual machine #2
Vm2{
  vmId = 2                                          //vmId here is accessed while testing Junit Test Case
  userId = 0                                        //userId here is accessed while testing Junit Test Case
  mips = 5000                                       //mips value
  pesNumber = 4                                     //number of cpus
  ram = 6144                                        //vm memory (MB)
  bw = 3000                                         //bandwidth
  size = 90000                                      //image size (MB)
  vmm = "Xen"                                       //VMM name
}

//Provides characteristics of cloudlet #1
Cloudlet1{
  length = 20000                                    //length or size (in MI)
  pesNumber = 1                                     //PEs number
  fileSize = 200
  outputSize = 200
}

//Provides characteristics of cloudlet #2
Cloudlet2{
  id = 2                                           //id here is accessed while testing Junit Test Case
  length = 40000                                   //length or size (in MI)
  pesNumber = 2                                    //PEs number
  fileSize = 400                                   //file size before submitting to Datacenter
  outputSize = 400                                 //file size of cloudlet after finish executing by Datacenter

}

//Provides characteristics of cloudlet #3
Cloudlet3{
  length = 1200000                                //length or size (in MI)
  pesNumber = 4                                   //PEs number
  fileSize = 800                                  //file size before submitting to Datacenter
  outputSize = 800                                //file size of cloudlet after finish executing by Datacenter
}

//Provides characteristics of cloudlet #4
Cloudlet4{
  length = 1300000                                //length or size (in MI)
  pesNumber = 4                                   //PEs number
  fileSize = 900                                  //file size before submitting to Datacenter
  outputSize = 900                                //file size of cloudlet after finish executing by Datacenter
}







