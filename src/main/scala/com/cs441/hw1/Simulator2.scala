package com.cs441.hw1

//Simulator 2: Models cloud computing environment for the cloud provider Amazon
//Submitted by: Joylyn Lewis
/*
Description: This simulator simulates a cloud environment based on the IaaS model with inputs and configuration variables defined in CloudProvider2.conf file.
> The file CloudProvider2.conf is assumed to contain parameters of the various cloud resources provided by Amazon along with parameters of the cloudlets that are
  to be executed in this cloud environment.
> The program Simulator2.scala customizes behavior of two classes (DatacenterBroker and Cloudlet) provided by CloudSim.
> The first class DatacenterBroker(which represents a broker that submits cloudlets to VMs) is extended to class
  CustomDataCenterBroker(which attempts at implementing load balancing concept by allocating higher length cloudlets to higher capacity VMS and
  lower length cloudlets to lower capacity VMS). This is implemented by overriding the function submitCloudlets()
> The second class Cloudlet (which represents the tasks to be executed) is extended to class CustomCloudlet to include execution cost along with the input
  data transfer and output transfer cost functionality in the function getProcessingCost()
*/
//Import required libraries
import com.typesafe.config._
import org.slf4j.LoggerFactory
import com.typesafe.scalalogging.Logger
import java.text.DecimalFormat
import java.util.Calendar
import java.util.List
import java.util

//Import cloudsim relevant libraries
import org.cloudbus.cloudsim.Cloudlet
import org.cloudbus.cloudsim.CloudletSchedulerTimeShared
import org.cloudbus.cloudsim.Datacenter
import org.cloudbus.cloudsim.DatacenterBroker
import org.cloudbus.cloudsim.DatacenterCharacteristics
import org.cloudbus.cloudsim.Host
import org.cloudbus.cloudsim.Pe
import org.cloudbus.cloudsim.Storage
import org.cloudbus.cloudsim.UtilizationModelFull
import org.cloudbus.cloudsim.Vm
import org.cloudbus.cloudsim.VmAllocationPolicySimple
import org.cloudbus.cloudsim.VmSchedulerTimeShared
import org.cloudbus.cloudsim.core.CloudSim
import org.cloudbus.cloudsim.core.CloudSimTags
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple
import org.cloudbus.cloudsim.UtilizationModel
import scala.collection.JavaConverters._

//Scala object Simulator2 represents the second Simulator program
object Simulator2 {

  //Class DatacenterBroker provided by CloudSim package is extended to provide custom functionality of load balancing by allocating higher length cloudlets to higher capacity VMS and
  //lower length cloudlets to lower capacity VMS
  class CustomDataCenterBroker(name: String) extends DatacenterBroker(name: String) {

    //Override function definition of submitCloudlets in inherited class to customize load balancing behavior
    override def submitCloudlets(): Unit = {

      val cloudletList = getCloudletList()                                                                              //Gets the cloudlet list
      val vmsCreatedList = getVmsCreatedList()                                                                          //Gets the created VMs list
      val cloudletLengthList = new util.ArrayList[Double](cloudletList.size())
      var vmIndex = 0                                                                                                   //vmIndex is iniitialized with value 0 as var which will be accessed once cloudlet is submitted
      cloudletList.forEach({ (e: Cloudlet) =>
        val length = e.getCloudletLength().toDouble                                                                     //Gets length of cloudlet
        cloudletLengthList.add(length)                                                                                  //Appends length of cloudlet to the ArrayList
      })

      val cloudletLengthArray = cloudletLengthList.asScala.toArray
      val sumofCloudletLength = cloudletLengthArray.reduceLeft(_ + _)                                                   //Adds lengths of all cloudlets
      val avgCloudletLength = sumofCloudletLength / cloudletList.size()                                                 //Calculates average cloudlet length

      val cloudletHigherLength = new util.ArrayList[Cloudlet](cloudletList.size())                                      //Create ArrayList for cloudlets with higher than average lengths
      val cloudletLowerLength = new util.ArrayList[Cloudlet](cloudletList.size())                                       //Create ArrayList for cloudlets with lower than average lengths

      //Classify the cloudlets into the two categories based on their lengths
      cloudletList.forEach({ (e: Cloudlet) =>
        val length = e.getCloudletLength().toDouble
        if (length >= avgCloudletLength)
          cloudletHigherLength.add(e)
        else
          cloudletLowerLength.add(e)
      })


      val vmMipsList = new util.ArrayList[Double](vmsCreatedList.size())
      vmsCreatedList.forEach({ (vm: Vm) =>
        val mips = vm.getMips()                                                                                         //Gets Mips of VMs
        vmMipsList.add(mips)                                                                                            //Appends mips of VMs to the ArrayList
      })

      val vmMipsArray = vmMipsList.asScala.toArray
      val sumOfVmMips = vmMipsArray.reduceLeft(_ + _)                                                                   //Adds Mips of all VMs
      val avgVmMips = sumOfVmMips / vmsCreatedList.size()                                                               //Calculates average VM Mips

      val vmHigherMips = new util.ArrayList[Vm](vmsCreatedList.size())                                                  //Create ArrayList for VMs with higher than average Mips
      val vmLowerMips = new util.ArrayList[Vm](vmsCreatedList.size())                                                   //Create ArrayList for VMs with lower than average Mips

      //Classify the VMs into the two categories based on their Mips
      vmsCreatedList.forEach({ (vm: Vm) => {
        val mips = vm.getMips()
        if (mips >= avgVmMips) vmHigherMips.add(vm)
        else vmLowerMips.add(vm)
      }
      }
      )

      //Allocate higher length cloudlets to higher capacity VM
      val vmHigherMipsArray = vmHigherMips.asScala.toArray
      val vmLowerMipsArray = vmLowerMips.asScala.toArray
      cloudletHigherLength.forEach({ e: Cloudlet => {
        val vm = vmHigherMipsArray.head

        e.setVmId(vm.getId())                                                                                           //Set the higher length VM id
        sendNow(getVmsToDatacentersMap().get(vm.getId()), CloudSimTags.CLOUDLET_SUBMIT, e)                              //Sends the event/message with tag representing the event type

        //Increments number of submitted cloudlets
        cloudletsSubmitted = cloudletsSubmitted + 1
        vmIndex = (vmIndex + 1) % getVmsCreatedList().size()
        getCloudletSubmittedList().add(e)
      }
      })

      //Allocate lower length cloudlets to lower capacity VM
      cloudletLowerLength.forEach({ e: Cloudlet =>
        val vm = vmLowerMipsArray.head
        e.setVmId(vm.getId())                                                                                           //Set the higher length VM id
        sendNow(getVmsToDatacentersMap().get(vm.getId()), CloudSimTags.CLOUDLET_SUBMIT, e)                              //Sends the event/message with tag representing the event type

        //Increments number of submitted cloudlets
        cloudletsSubmitted = cloudletsSubmitted + 1
        vmIndex = (vmIndex + 1) % getVmsCreatedList().size()
        getCloudletSubmittedList().add(e)
      })

      //Removes each cloudlet from the submitted list
      val cloudletSubmittedList = getCloudletSubmittedList()
      cloudletSubmittedList.forEach { (e: Cloudlet) =>
        getCloudletList().remove(e)
      }

    }
  }
  //Class Cloudlet provided by CloudSim package is extended to include custom functionality of adding execution cost of cloudlet to the input data transfer and output transfer cost
  class CustomCloudlet(cloudletId: Int, cloudletLength: Long, pesNumber: Int, cloudletFileSize: Long, cloudletOutputSize: Long, utilizationModelCpu: UtilizationModel, utilizationModelRam: UtilizationModel, utilizationModelBw: UtilizationModel)
    extends Cloudlet(cloudletId: Int, cloudletLength: Long, pesNumber: Int, cloudletFileSize: Long, cloudletOutputSize: Long, utilizationModelCpu: UtilizationModel, utilizationModelRam: UtilizationModel, utilizationModelBw: UtilizationModel) {

    //Override function definition of getProcessingCost in inherited class to include execution cost of cloudlet to the input data transfer and output transfer cost
    override def getProcessingCost(): Double = {
      var totalCost: Double = 0                                                                                         //Variable totalCost is initialized as var as input transfer cost, execution cost and output transfer cost will be added

      if (getCloudletStatus() == Cloudlet.SUCCESS) {                                                                    //Cost will be calculated only if the cloudlet is successfully run

        totalCost += accumulatedBwCost + (costPerBw * getCloudletOutputSize)                                            //Add the input and output transfer costs

        val resourceArray = getAllResourceId()                                                                          //Gets all the CloudResource IDs that executed this cloudlet

        resourceArray.foreach(res => {
          val costPerSec = getCostPerSec((res))                                                                         //Gets the cost running this cloudlet in a given CloudResource ID
          val actualCpuTime = getActualCPUTime(res)                                                                     //Gets the total execution time of this cloudlet in a given CloudResource ID
          totalCost += (costPerSec * actualCpuTime)                                                                     //Calculate total cost of executing cloudlet in a given CloudResource ID

        })

      }
      val config = ConfigFactory.load("CloudProvider2.conf")                                           //Load the application's configuration from the classpath resource basename
      BigDecimal(totalCost).setScale(config.getString("CloudSim.decimalRoundOff").toInt,                         //Return totalCost with limited decimal places based on parameter
        BigDecimal.RoundingMode.HALF_UP).toDouble                                                                       //'CloudSim.decimalRoundOff' set in the application's configuration

    }
  }


  def main(args: Array[String]): Unit = {
    val config = ConfigFactory.load("CloudProvider2.conf")                                             //Load the application's configuration from the classpath resource basename

    val logger = Logger(LoggerFactory.getLogger(config.getString("CloudSim.logger")))                            //Create a logger instance for supplying logging statements


    val cal = Calendar.getInstance()                                                                                    //Initialize the CloudSim package
    val numUser = config.getString("CloudSim.noOfCloudUsers").toInt                                              //Set number of cloud users
    val traceFlag = config.getString("CloudSim.traceFlag").toBoolean                                             //Set flag for trace events

    logger.info("Simulation 2 is for cloud provider - Amazon")                                                          //Log start of simulation

    try {

      CloudSim.init(numUser, cal, traceFlag)                                                                            // Initialize the CloudSim library
      logger.info("Success at Initializing")                                                                            // Log success of initializing

      val datacenter1 = createDatacenter(config.getString("DatacenterParameters.Name1"))                         //Create two datacenters
      val datacenter2 = createDatacenter(config.getString("DatacenterParameters.Name2"))

      val broker = createBroker()                                                                                       //Create Broker
      val brokerId = broker.getId()

      val vmList = new util.ArrayList[Vm]                                                                               //Create list of virtual machines
      val vmIds: Array[Int] = config.getString("DatacenterParameters.vmMin").toInt to                            //Create Ids of VMs based on minimum and maximum number of VMS required
        config.getString("DatacenterParameters.vmMax").toInt toArray

      vmIds.foreach(vmId =>
        vmList.add(                                                                                                     //Create required number of VMs and add to vmList
          new Vm(vmId, brokerId, config.getString(s"Vm$vmId.mips").toInt,
            config.getString(s"Vm$vmId.pesNumber").toInt, config.getString(s"Vm$vmId.ram").toInt, config.getString(s"Vm$vmId.bw").toLong,
            config.getString(s"Vm$vmId.size").toLong, config.getString(s"Vm$vmId.vmm"), new CloudletSchedulerTimeShared()))
      )

      broker.submitVmList(vmList)                                                                                       //Submit vmList to Broker

      val cloudletList = new util.ArrayList[Cloudlet]                                                                   //Create list of cloudlets
      val utilizationModel = new UtilizationModelFull()
      val cloudletIds: Array[Int] = config.getString("CloudSim.cloudletMin").toInt to                            //Create Ids of cloudlets based on minimum and maximum number of cloudlets required
        config.getString("CloudSim.cloudletMax").toInt toArray

      cloudletIds.foreach(cloudletId => {                                                                               //Create required number of cloudlets and add to list
        val cloudlet = new CustomCloudlet(cloudletId, config.getString(s"Cloudlet$cloudletId.length").toLong, config.getString(s"Cloudlet$cloudletId.pesNumber").toInt,
          config.getString(s"Cloudlet$cloudletId.fileSize").toLong, config.getString(s"Cloudlet$cloudletId.outputSize").toLong, utilizationModel, utilizationModel, utilizationModel)
        cloudlet.setUserId(brokerId)
        cloudletList.add(cloudlet)
      }

      )

      broker.submitCloudletList(cloudletList)                                                                           //Submit cloudlet list to Broker

      CloudSim.startSimulation                                                                                          //Start the simulation

      CloudSim.stopSimulation()                                                                                         //Stop the simulation

      val newList: List[Cloudlet] = broker.getCloudletReceivedList()                                                    //Gets the cloudlet received list
      printCloudletList(newList)                                                                                        //Print results when simulation is over
      logger.info("Simulator 2 for cloud provider Amazon is finished!")                                                 //Log completed status of simulation
    }
    catch {
      case e: Exception => e.printStackTrace
        logger.error("The simulation has been terminated due to an unexpected error")                                   //Log error in case of exception
    }

  }

  //Function to create datacenter
  def createDatacenter(datacenterName: String): Datacenter = {

    val hostList = new util.ArrayList[Host]                                                                             //Create list to store the hosts
    val peList = new util.ArrayList[Pe]                                                                                 //Create list to store PEs contained on a machine

    val config = ConfigFactory.load("CloudProvider2.conf")                                             //Load the application's configuration from the classpath resource basename
    val logger = Logger(LoggerFactory.getLogger(config.getString("CloudSim.logger")))                            //Create a logger instance for supplying logging statements

    val peIds: Array[Int] = config.getString("DatacenterParameters.peMin").toInt to                              //Create Ids of PEs based on minimum and maximum number of required PEs
      config.getString("DatacenterParameters.peMax").toInt toArray


    peIds.foreach(peId =>
      peList.add(                                                                                                       //Add PEs created into list
        new Pe(peId, new PeProvisionerSimple(config.getString("DatacenterParameters.mips").toInt)))
    )

    val hostIds: Array[Int] = config.getString("DatacenterParameters.hostMin").toInt to                          //Create Ids of hosts based on minimum and maximum number of required hosts
      config.getString("DatacenterParameters.hostMax").toInt toArray

    hostIds.foreach(hostId =>
      hostList.add(                                                                                                     //Create host with its id and list of PEs and add them to the list of machines
        new Host(
          hostId,
          new RamProvisionerSimple(config.getString((s"$datacenterName.ram")).toInt),
          new BwProvisionerSimple(config.getString((s"$datacenterName.bw")).toInt),
          config.getString((s"$datacenterName.storage")).toLong,
          peList,
          new VmSchedulerTimeShared(peList)
        )
      )
    )
    //Create a DatacenterCharacteristics object that stores the properties of a data center: architecture, OS, list of machines, allocation policy, time zone and its price
    val characteristics = new DatacenterCharacteristics(config.getString((s"$datacenterName.arch")), config.getString((s"$datacenterName.os")),
      config.getString((s"$datacenterName.vmm")), hostList, config.getString((s"$datacenterName.timeZone")).toDouble, config.getString((s"$datacenterName.cost")).toDouble,
      config.getString((s"$datacenterName.costPerMem")).toDouble, config.getString((s"$datacenterName.costPerStorage")).toDouble, config.getString((s"$datacenterName.costPerBw")).toDouble
    )
    val storageList = new util.LinkedList[Storage]
    //Create datacenter
    val datacenter = new Datacenter(datacenterName, characteristics, new VmAllocationPolicySimple(hostList), storageList, config.getString("DatacenterParameters.schedulingInterval").toInt)
    datacenter                                                                                                          //Return the datacenter object

  }

  //Function to create Broker
  def createBroker(): DatacenterBroker = {
    val config = ConfigFactory.load("CloudProvider2.conf")                                             //Load the application's configuration from the classpath resource basename
    val broker = new CustomDataCenterBroker(config.getString("CloudSim.brokerName"))                             //Create customized broker
    broker
  }

  //Function to output list of cloudlets
  def printCloudletList(list: List[Cloudlet]): Unit = {
    val config = ConfigFactory.load("CloudProvider2.conf")                                             //Load the application's configuration from the classpath resource basename
    val indent = config.getString("CloudSim.indent")                                                             //Load formatting pattern

    val logger = Logger(LoggerFactory.getLogger(config.getString("CloudSim.logger")))                            //Create a logger instance for supplying logging statements

    //Log output patterns
    logger.info(config.getString("CloudSim.output"))

    logger.info("Cloudlet ID" + indent + "STATUS" + indent + "Data center ID" + indent + "VM ID" + indent + indent + "Time" + indent + indent + "Start Time" + indent + "Finish Time"
      + indent + "Total cost of processing cloudlet")

    val dft = new DecimalFormat(config.getString("CloudSim.pattern"))
    list.forEach { (e: Cloudlet) =>                                                                                     //Log outputs for each cloudlet
      if (e.getCloudletStatus() == Cloudlet.SUCCESS)
        logger.info(indent + e.getCloudletId + indent + indent + config.getString("CloudSim.success") + indent + indent + e.getResourceId() + indent + indent + indent + indent + e.getVmId() + indent + indent + indent + dft.format(e.getActualCPUTime()) + indent + indent + indent + dft.format(e.getExecStartTime()) + indent + indent + indent + dft.format(e.getFinishTime())
          + indent + indent + e.getProcessingCost())
      else
        logger.info(indent + e.getCloudletId + indent + indent + config.getString("CloudSim.fail") + indent + indent + e.getResourceId() + indent + indent + indent + indent + e.getVmId() + indent + indent + indent + dft.format(e.getActualCPUTime()) + indent + indent + indent + dft.format(e.getExecStartTime()) + indent + indent + indent + dft.format(e.getFinishTime())
          + indent + indent + e.getProcessingCost())
    }
  }
}

