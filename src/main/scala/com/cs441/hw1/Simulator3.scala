package com.cs441.hw1

//Simulator 3: Models cloud computing environment for the cloud provider Microsoft
//Submitted by: Joylyn Lewis
/*
Description: This simulator simulates a cloud environment based on the IaaS model with inputs and configuration variables defined in CloudProvider3.conf file.
> The file CloudProvider3.conf is assumed to contain parameters of the various cloud resources provided by Microsoft along with parameters of the cloudlets that are
  to be executed in this cloud environment.
> The program Simulator3.scala customizes behavior of one class (Cloudlet) provided by CloudSim.
> The class Cloudlet (which represents the tasks to be executed) is extended to class CustomCloudlet to include execution cost along with the input
  data transfer and output transfer cost functionality in the function getProcessingCost()
*/

//Import required libraries
import com.typesafe.config._
import org.slf4j.LoggerFactory
import com.typesafe.scalalogging.Logger
import java.text.DecimalFormat
import java.util.Calendar
import java.util.List
import java.util

//Import cloudsim relevant libraries
import org.cloudbus.cloudsim.Cloudlet
import org.cloudbus.cloudsim.CloudletSchedulerTimeShared
import org.cloudbus.cloudsim.Datacenter
import org.cloudbus.cloudsim.DatacenterBroker
import org.cloudbus.cloudsim.DatacenterCharacteristics
import org.cloudbus.cloudsim.Host
import org.cloudbus.cloudsim.Pe
import org.cloudbus.cloudsim.Storage
import org.cloudbus.cloudsim.UtilizationModelFull
import org.cloudbus.cloudsim.Vm
import org.cloudbus.cloudsim.VmAllocationPolicySimple
import org.cloudbus.cloudsim.VmSchedulerTimeShared
import org.cloudbus.cloudsim.core.CloudSim
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple
import org.cloudbus.cloudsim.UtilizationModel

//Scala object Simulator3 represents the third Simulator program
object Simulator3 {

  //Class Cloudlet provided by CloudSim package is extended to include custom functionality of adding execution cost of cloudlet to the input data transfer and output transfer cost
  class CustomCloudlet(cloudletId: Int, cloudletLength: Long, pesNumber: Int, cloudletFileSize: Long, cloudletOutputSize: Long, utilizationModelCpu: UtilizationModel, utilizationModelRam: UtilizationModel, utilizationModelBw: UtilizationModel)
    extends Cloudlet(cloudletId: Int, cloudletLength: Long, pesNumber: Int, cloudletFileSize: Long, cloudletOutputSize: Long, utilizationModelCpu: UtilizationModel, utilizationModelRam: UtilizationModel, utilizationModelBw: UtilizationModel) {

    //Override function definition of getProcessingCost in inherited class to include execution cost of cloudlet to the input data transfer and output transfer cost
    override def getProcessingCost(): Double = {
      var totalCost: Double = 0                                                                                         //Variable totalCost is initialized as var as input transfer cost, execution cost and output transfer cost will be added

      if (getCloudletStatus() == Cloudlet.SUCCESS) {                                                                    //Cost will be calculated only if the cloudlet is successfully run

        totalCost += accumulatedBwCost + (costPerBw * getCloudletOutputSize)                                            //Add the input and output transfer costs

        val resourceArray = getAllResourceId()                                                                          //Gets all the CloudResource IDs that executed this cloudlet

        resourceArray.foreach(res => {
          val costPerSec = getCostPerSec((res))                                                                         //Gets the cost running this cloudlet in a given CloudResource ID
          val actualCpuTime = getActualCPUTime(res)                                                                     //Gets the total execution time of this cloudlet in a given CloudResource ID
          totalCost += (costPerSec * actualCpuTime)                                                                     //Calculate total cost of executing cloudlet in a given CloudResource ID

        })

      }
      val config = ConfigFactory.load("CloudProvider2.conf")                                           //Load the application's configuration from the classpath resource basename
      BigDecimal(totalCost).setScale(config.getString("CloudSim.decimalRoundOff").toInt,                         //Return totalCost with limited decimal places based on parameter
        BigDecimal.RoundingMode.HALF_UP).toDouble                                                                       //'CloudSim.decimalRoundOff' set in the application's configuration

    }
  }


  def main(args: Array[String]): Unit = {
    val config = ConfigFactory.load("CloudProvider3.conf")                                             //Load the application's configuration from the classpath resource basenam

    val logger = Logger(LoggerFactory.getLogger(config.getString("CloudSim.logger")))                            //Create a logger instance for supplying logging statements

    val cal = Calendar.getInstance()                                                                                    //Initialize the CloudSim package
    val numUser = config.getString("CloudSim.noOfCloudUsers").toInt                                              //Set number of cloud users
    val traceFlag = config.getString("CloudSim.traceFlag").toBoolean                                             //Set flag for trace events

    logger.info("Simulation 3 is for cloud provider - Microsoft")                                                       //Log start of simulation

    try {

      CloudSim.init(numUser, cal, traceFlag)                                                                            // Initialize the CloudSim library
      logger.info("Success at Initializing")                                                                            // Log success of initializing


      val datacenter1 = createDatacenter(config.getString("DatacenterParameters.Name1"))                         //Create two datacenters
      val datacenter2 = createDatacenter(config.getString("DatacenterParameters.Name2"))

      val broker = createBroker()                                                                                       //Create Broker
      val brokerId = broker.getId()

      val vmList = new util.ArrayList[Vm]                                                                               //Create list of virtual machines
      val vmIds: Array[Int] = config.getString("DatacenterParameters.vmMin").toInt to                            //Create Ids of VMs based on minimum and maximum number of VMS required
        config.getString("DatacenterParameters.vmMax").toInt toArray


      vmIds.foreach(vmId =>
        vmList.add(                                                                                                     //Create required number of VMs and add to vmList
          new Vm(vmId, brokerId, config.getString(s"Vm$vmId.mips").toInt,
            config.getString(s"Vm$vmId.pesNumber").toInt, config.getString(s"Vm$vmId.ram").toInt, config.getString(s"Vm$vmId.bw").toLong,
            config.getString(s"Vm$vmId.size").toLong, config.getString(s"Vm$vmId.vmm"), new CloudletSchedulerTimeShared()))
      )

      broker.submitVmList(vmList)                                                                                       //Submit vmList to Broker

      val cloudletList = new util.ArrayList[Cloudlet]                                                                   //Create list of cloudlets
      val utilizationModel = new UtilizationModelFull()

      val cloudletIds: Array[Int] = config.getString("CloudSim.cloudletMin").toInt to                            //Create Ids of cloudlets based on minimum and maximum number of cloudlets required
        config.getString("CloudSim.cloudletMax").toInt toArray

      cloudletIds.foreach(cloudletId => {                                                                               //Create required number of cloudlets and add to list
        val cloudlet = new CustomCloudlet(cloudletId, config.getString(s"Cloudlet$cloudletId.length").toLong, config.getString(s"Cloudlet$cloudletId.pesNumber").toInt,
          config.getString(s"Cloudlet$cloudletId.fileSize").toLong, config.getString(s"Cloudlet$cloudletId.outputSize").toLong, utilizationModel, utilizationModel, utilizationModel)
        cloudlet.setUserId(brokerId)
        cloudletList.add(cloudlet)
      }
      )

      broker.submitCloudletList(cloudletList)                                                                           //Submit cloudlet list to Broker

      CloudSim.startSimulation                                                                                          //Start the simulation

      CloudSim.stopSimulation()                                                                                         //Stop the simulation

      val newList: List[Cloudlet] = broker.getCloudletReceivedList()                                                    //Gets the cloudlet received list
      printCloudletList(newList)                                                                                        //Print results when simulation is over
      logger.info("Simulator 3 for cloud provider Microsoft is finished!")                                              //Log completed status of simulation
    }
    catch {
      case e: Exception => e.printStackTrace
        logger.error("The simulation has been terminated due to an unexpected error")                                   //Log error in case of exception
    }
  }

  //Function to create datacenter
  def createDatacenter(datacenterName: String): Datacenter = {

    val hostList = new util.ArrayList[Host]                                                                             //Create list to store the hosts
    val peList = new util.ArrayList[Pe]                                                                                 //Create list to store PEs contained on a machine

    val config = ConfigFactory.load("CloudProvider3.conf")                                             //Load the application's configuration from the classpath resource basename
    val logger = Logger(LoggerFactory.getLogger(config.getString("CloudSim.logger")))                            //Create a logger instance for supplying logging statements

    val peIds: Array[Int] = config.getString("DatacenterParameters.peMin").toInt to                              //Create Ids of PEs based on minimum and maximum number of required PEs
      config.getString("DatacenterParameters.peMax").toInt toArray

    peIds.foreach(peId =>
      peList.add(                                                                                                       //Add PEs created into list
        new Pe(peId, new PeProvisionerSimple(config.getString("DatacenterParameters.mips").toInt)))
    )

    val hostIds: Array[Int] = config.getString("DatacenterParameters.hostMin").toInt to                          //Create Ids of hosts based on minimum and maximum number of required
      config.getString("DatacenterParameters.hostMax").toInt toArray

    hostIds.foreach(hostId =>
      hostList.add(                                                                                                     //Create host with its id and list of PEs and add them to the list of machines
        new Host(
          hostId,
          new RamProvisionerSimple(config.getString((s"$datacenterName.ram")).toInt),
          new BwProvisionerSimple(config.getString((s"$datacenterName.bw")).toInt),
          config.getString((s"$datacenterName.storage")).toLong,
          peList,
          new VmSchedulerTimeShared(peList)
        )
      )
    )
    //Create a DatacenterCharacteristics object that stores the properties of a data center: architecture, OS, list of machines, allocation policy, time zone and its price
    val characteristics = new DatacenterCharacteristics(config.getString((s"$datacenterName.arch")), config.getString((s"$datacenterName.os")),
      config.getString((s"$datacenterName.vmm")), hostList, config.getString((s"$datacenterName.timeZone")).toDouble, config.getString((s"$datacenterName.cost")).toDouble,
      config.getString((s"$datacenterName.costPerMem")).toDouble, config.getString((s"$datacenterName.costPerStorage")).toDouble, config.getString((s"$datacenterName.costPerBw")).toDouble
    )
    val storageList = new util.LinkedList[Storage]
    //Create datacenter
    val datacenter = new Datacenter(datacenterName, characteristics, new VmAllocationPolicySimple(hostList), storageList, config.getString("DatacenterParameters.schedulingInterval").toInt)
    datacenter

  }
  //Function to create Broker
  def createBroker(): DatacenterBroker = {
    val config = ConfigFactory.load("CloudProvider3.conf")                                             //Load the application's configuration from the classpath resource basename
    val broker = new DatacenterBroker(config.getString("CloudSim.brokerName"))                                   //Create broker
    broker
  }

  //Function to output list of cloudlets
  def printCloudletList(list: List[Cloudlet]): Unit = {
    val config = ConfigFactory.load("CloudProvider3.conf")                                             //Load the application's configuration from the classpath resource basename
    val indent = config.getString("CloudSim.indent")                                                             //Load formatting pattern

    val logger = Logger(LoggerFactory.getLogger(config.getString("CloudSim.logger")))                            //Create a logger instance for supplying logging statements

    //Log output patterns
    logger.info(config.getString("CloudSim.output"))

    logger.info("Cloudlet ID" + indent + "STATUS" + indent + "Data center ID" + indent + "VM ID" + indent + indent + "Time" + indent + indent + "Start Time" + indent + "Finish Time"
      + indent + "Total cost of processing cloudlet")

    val dft = new DecimalFormat(config.getString("CloudSim.pattern"))
    list.forEach { (e: Cloudlet) =>                                                                                     //Log outputs for each cloudlet
      if (e.getCloudletStatus() == Cloudlet.SUCCESS)
        logger.info(indent + e.getCloudletId + indent + indent + config.getString("CloudSim.success") + indent + indent + e.getResourceId() + indent + indent + indent + indent + e.getVmId() + indent + indent + indent + dft.format(e.getActualCPUTime()) + indent + indent + indent + dft.format(e.getExecStartTime()) + indent + indent + indent + dft.format(e.getFinishTime())
          + indent + indent + e.getProcessingCost())
      else
        logger.info(indent + e.getCloudletId + indent + indent + config.getString("CloudSim.fail") + indent + indent + e.getResourceId() + indent + indent + indent + indent + e.getVmId() + indent + indent + indent + dft.format(e.getActualCPUTime()) + indent + indent + indent + dft.format(e.getExecStartTime()) + indent + indent + indent + dft.format(e.getFinishTime())
          + indent + indent + e.getProcessingCost())
    }
  }
}

