package com.cs441.hw1

// Unit Tests for Simulator2 with configuration variables and input provided via 'CloudProvider2.conf'
//Submitted by: Joylyn Lewis

//Import required libraries
import org.junit.Assert._
import org.junit.Test
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

class Simulator2Test {
  //define config variable to read input values
  val config =  com.typesafe.config.ConfigFactory.load("CloudProvider2.conf")

  //Unit Test 1: Test whether PE is created successfully
  @Test
  def peTest : Unit = {
    val pe = new org.cloudbus.cloudsim.Pe(config.getString("DatacenterParameters.peMin").toInt, new org.cloudbus.cloudsim.provisioners.PeProvisionerSimple(config.getString("DatacenterParameters.mips").toInt))
    assertNotNull(pe)
  }

  //Unit Test 2: Test whether Cloudlet2 is created successfully
  @Test
  def CloudletTest: Unit = {
    val utilizationModel = new org.cloudbus.cloudsim.UtilizationModelFull()
    val cloudlet2 = new org.cloudbus.cloudsim.Cloudlet(config.getString("Cloudlet2.id").toInt, config.getString("Cloudlet2.length").toLong, config.getString("Cloudlet2.pesNumber").toInt,
      config.getString("Cloudlet2.fileSize").toLong, config.getString("Cloudlet2.outputSize").toLong, utilizationModel, utilizationModel, utilizationModel)
    assertNotNull(cloudlet2)
  }

  //Unit test 3: Test whether VM2 is created successfully
  @Test
  def vmTest : Unit = {
    val vm2 = new org.cloudbus.cloudsim.Vm(config.getString("Vm2.vmId").toInt, config.getString("Vm2.userId").toInt, config.getString("Vm2.mips").toInt,
      config.getString("Vm2.pesNumber").toInt, config.getString("Vm2.ram").toInt, config.getString("Vm2.bw").toLong,
      config.getString("Vm2.size").toLong, config.getString("Vm2.vmm"), new org.cloudbus.cloudsim.CloudletSchedulerTimeShared())
    assertNotNull(vm2)
  }

  //Unit Test 4: Test whether logger object is created successfully
  @Test
  def loggerTest : Unit = {
    val logger = Logger(LoggerFactory.getLogger(config.getString("CloudSim.logger")))
    assertNotNull(logger)

  }
  //Unit Test 5: Test whether values can be read correctly from the input file 'CloudProvider2.conf'
  @Test
  def ConfigCloudletMaxTest: Unit = {
    val cloudletMax = config.getString("CloudSim.cloudletMax").toDouble
    assertTrue( cloudletMax == 4)

  }

}

