package com.cs441.hw1

// Unit Tests for Simulator1 with configuration variables and input provided via 'CloudProvider1.conf'
//Submitted by: Joylyn Lewis

//Import required libraries
import org.junit.Assert._
import org.junit.Test
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

class Simulator1Test {
  //define config variable to read input values
  val config =  com.typesafe.config.ConfigFactory.load("CloudProvider1.conf")

  //Unit test 1: Test whether VM1 is created successfully
  @Test
  def vmTest : Unit = {
    val vm1 = new org.cloudbus.cloudsim.Vm(config.getString("Vm1.vmId").toInt, config.getString("Vm1.userId").toInt, config.getString("Vm1.mips").toInt,
      config.getString("Vm1.pesNumber").toInt, config.getString("Vm1.ram").toInt, config.getString("Vm1.bw").toLong,
      config.getString("Vm1.size").toLong, config.getString("Vm1.vmm"), new org.cloudbus.cloudsim.CloudletSchedulerTimeShared())
    assertNotNull(vm1)
  }
  //Unit Test 2: Test whether PE is created successfully
  @Test
  def peTest : Unit = {
    val pe = new org.cloudbus.cloudsim.Pe(config.getString("DatacenterParameters.peMin").toInt, new org.cloudbus.cloudsim.provisioners.PeProvisionerSimple(config.getString("DatacenterParameters.mips").toInt))
    assertNotNull(pe)
  }
  //Unit Test 3: Test whether logger object is created successfully
  @Test
  def loggerTest : Unit = {
    val logger = Logger(LoggerFactory.getLogger(config.getString("CloudSim.logger")))
    assertNotNull(logger)

  }
  //Unit Test 4: Test whether values can be read correctly from the input file 'CloudProvider1.conf'
  @Test
  def ConfigMipsTest: Unit = {
    val mips = config.getString("DatacenterParameters.mips").toDouble
    assertTrue( mips == 40000)

  }
  //Unit Test 5: Test whether Cloudlet1 is created successfully
  @Test
  def CloudletTest: Unit = {
    val utilizationModel = new org.cloudbus.cloudsim.UtilizationModelFull()
    val cloudlet1 = new org.cloudbus.cloudsim.Cloudlet(config.getString("Cloudlet1.id").toInt, config.getString("Cloudlet1.length").toLong, config.getString("Cloudlet1.pesNumber").toInt,
      config.getString("Cloudlet1.fileSize").toLong, config.getString("Cloudlet1.outputSize").toLong, utilizationModel, utilizationModel, utilizationModel)
    assertNotNull(cloudlet1)
  }



}

