package com.cs441.hw1

// Unit Tests for Simulator3 with configuration variables and input provided via 'CloudProvider3.conf'
//Submitted by: Joylyn Lewis

//Import required libraries
import org.junit.Assert._
import org.junit.Test
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

class Simulator3Test {
  //define config variable to read input values
  val config =  com.typesafe.config.ConfigFactory.load("CloudProvider3.conf")

  //Unit Test 1: Test whether values can be read correctly from the input file 'CloudProvider3.conf'
  @Test
  def ConfigCloudletMaxTest: Unit = {
    val datacenter1Ram = config.getString("Datacenter_1.ram").toDouble
    assertTrue( datacenter1Ram == 4096)
  }

  //Unit test 2: Test whether VM3 is created successfully
  @Test
  def vmTest : Unit = {
    val vm3 = new org.cloudbus.cloudsim.Vm(config.getString("Vm3.vmId").toInt, config.getString("Vm3.userId").toInt, config.getString("Vm3.mips").toInt,
      config.getString("Vm3.pesNumber").toInt, config.getString("Vm3.ram").toInt, config.getString("Vm3.bw").toLong,
      config.getString("Vm3.size").toLong, config.getString("Vm3.vmm"), new org.cloudbus.cloudsim.CloudletSchedulerTimeShared())
    assertNotNull(vm3)
  }

  //Unit Test 3: Test whether Cloudlet3 is created successfully
  @Test
  def CloudletTest: Unit = {
    val utilizationModel = new org.cloudbus.cloudsim.UtilizationModelFull()
    val cloudlet3 = new org.cloudbus.cloudsim.Cloudlet(config.getString("Cloudlet3.id").toInt, config.getString("Cloudlet3.length").toLong, config.getString("Cloudlet3.pesNumber").toInt,
      config.getString("Cloudlet3.fileSize").toLong, config.getString("Cloudlet3.outputSize").toLong, utilizationModel, utilizationModel, utilizationModel)
    assertNotNull(cloudlet3)
  }

  //Unit Test 4: Test whether logger object is created successfully
  @Test
  def loggerTest : Unit = {
    val logger = Logger(LoggerFactory.getLogger(config.getString("CloudSim.logger")))
    assertNotNull(logger)
  }

  //Unit Test 5: Test whether PE is created successfully
  @Test
  def peTest : Unit = {
    val pe = new org.cloudbus.cloudsim.Pe(config.getString("DatacenterParameters.peMin").toInt, new org.cloudbus.cloudsim.provisioners.PeProvisionerSimple(config.getString("DatacenterParameters.mips").toInt))
    assertNotNull(pe)
  }

}



